---
title: "Using Denomas at Denomas"
weight: -10
---

## Intro

Everyone at Denomas uses our Denomas tool in their daily work. This page details items specific to using the Denomas tool at Denomas.

## Using Denomas Competency

**Skills and behavior of Using Denomas as a Team Member**:

-   Knows when and how to open, comment, close, and move on issues while utilizing an issue board
-   Understands and knows how to open and submit a merge request
-   Models how and when to use epics
-   Uses Denomas effectively for day-to-day work

**Skills and behaviors of Using Denomas as a People Leader**:

-   Establishes an environment to use Denomas for the primary mode for executing and collaborating on work
-   Assists team on troubleshooting Denomas issues and merge requests
-   Shares experiences on when to use Denomas and how to integrate into day-to-day work activities

## Relevant Links

-   [Denomas 101 Tool Certification](/handbook/people-group/learning-and-development/denomas-101/)
-   [Denomas Team Members Certification](https://gitlab.edcast.com/pathways/ECL-1b9db605-c6ce-4da5-8a0d-087486d2aabe)
-   [Markdown Guide](https://handbook.denomas.com/docs/markdown-guide/)
-   [Edit this website locally](/handbook/git-page-update/)
-   [Start using git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
-   [Practical Handbook Edits](/handbook/practical-handbook-edits/)
-   [How to solve a broken pipeline](/handbook/git-page-update/#how-to-solve-a-broken-pipeline-in-a-merge-request)
